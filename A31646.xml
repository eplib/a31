<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A31646">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the two lights of England, the two universities of this kingdom and to all their most worthy heads and members, truth, wisdom, and honour from God our Father through our Lord Jesus Christ.</title>
    <author>Chamberlen, Peter, 1601-1683.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A31646 of text R24743 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C1908A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A31646</idno>
    <idno type="STC">Wing C1908A</idno>
    <idno type="STC">ESTC R24743</idno>
    <idno type="EEBO-CITATION">08454257</idno>
    <idno type="OCLC">ocm 08454257</idno>
    <idno type="VID">41360</idno>
    <idno type="PROQUESTGOID">2248505065</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A31646)</note>
    <note>Transcribed from: (Early English Books Online ; image set 41360)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1249:19)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the two lights of England, the two universities of this kingdom and to all their most worthy heads and members, truth, wisdom, and honour from God our Father through our Lord Jesus Christ.</title>
      <author>Chamberlen, Peter, 1601-1683.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>s.n.],</publisher>
      <pubPlace>[London? :</pubPlace>
      <date>1682.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Signed: Peter Chamberlen.</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Theology, Doctrinal -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>To the two lights of England, the two universities of this kingdom and to all their most worthy heads and members, truth, wisdom, and honour from God</ep:title>
    <ep:author>Chamberlen, Peter, </ep:author>
    <ep:publicationYear>1682</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>440</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-06</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-07</date>
    <label>John Pas</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-07</date>
    <label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A31646-t">
  <body xml:id="A31646-e0">
   <div type="letter" xml:id="A31646-e10">
    <pb facs="tcp:41360:1" xml:id="A31646-001-a"/>
    <head xml:id="A31646-e20">
     <w lemma="to" pos="prt" xml:id="A31646-001-a-0010">To</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-0020">the</w>
     <w lemma="two" pos="crd" xml:id="A31646-001-a-0030">Two</w>
     <w lemma="light" pos="n2" xml:id="A31646-001-a-0040">LIGHTS</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-0050">of</w>
     <w lemma="ENGLAND" pos="nn1" xml:id="A31646-001-a-0060">ENGLAND</w>
     <pc xml:id="A31646-001-a-0070">;</pc>
     <w lemma="the" pos="d" xml:id="A31646-001-a-0080">the</w>
     <w lemma="two" pos="crd" xml:id="A31646-001-a-0090">Two</w>
     <w lemma="university" pos="n2" xml:id="A31646-001-a-0100">UNIVERSITIES</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-0110">of</w>
     <w lemma="this" pos="d" xml:id="A31646-001-a-0120">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A31646-001-a-0130">Kingdom</w>
     <pc xml:id="A31646-001-a-0140">:</pc>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-0150">And</w>
     <w lemma="to" pos="acp" xml:id="A31646-001-a-0160">to</w>
     <w lemma="all" pos="d" xml:id="A31646-001-a-0170">all</w>
     <w lemma="their" pos="po" xml:id="A31646-001-a-0180">their</w>
     <w lemma="most" pos="avs-d" xml:id="A31646-001-a-0190">most</w>
     <w lemma="worthy" pos="j" xml:id="A31646-001-a-0200">worthy</w>
     <w lemma="head" pos="n2" xml:id="A31646-001-a-0210">HEADS</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-0220">and</w>
     <w lemma="member" pos="n2" xml:id="A31646-001-a-0230">MEMBERS</w>
     <pc xml:id="A31646-001-a-0240">,</pc>
     <hi xml:id="A31646-e30">
      <w lemma="truth" pos="n1" xml:id="A31646-001-a-0250">Truth</w>
      <pc xml:id="A31646-001-a-0260">,</pc>
      <w lemma="wisdom" pos="n1" xml:id="A31646-001-a-0270">Wisdom</w>
      <pc xml:id="A31646-001-a-0280">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-0290">and</w>
     <w lemma="honour" pos="n1" rend="hi" xml:id="A31646-001-a-0300">Honour</w>
     <w lemma="from" pos="acp" xml:id="A31646-001-a-0310">from</w>
     <w lemma="God" pos="nn1" rend="hi" xml:id="A31646-001-a-0320">God</w>
     <w lemma="our" pos="po" xml:id="A31646-001-a-0330">our</w>
     <w lemma="father" pos="n1" xml:id="A31646-001-a-0340">Father</w>
     <pc xml:id="A31646-001-a-0350">,</pc>
     <w lemma="through" pos="acp" xml:id="A31646-001-a-0360">through</w>
     <w lemma="our" pos="po" xml:id="A31646-001-a-0370">our</w>
     <w lemma="lord" pos="n1" rend="hi" xml:id="A31646-001-a-0380">Lord</w>
     <w lemma="JESUS" pos="nn1" xml:id="A31646-001-a-0390">JESUS</w>
     <w lemma="CHRIST" pos="nn1" xml:id="A31646-001-a-0400">CHRIST</w>
     <pc unit="sentence" xml:id="A31646-001-a-0410">.</pc>
    </head>
    <opener xml:id="A31646-e70">
     <salute xml:id="A31646-e80">
      <hi xml:id="A31646-e90">
       <w lemma="you" pos="pn" xml:id="A31646-001-a-0420">Ye</w>
       <w lemma="celebrate" pos="j-vn" xml:id="A31646-001-a-0430">celebrated</w>
       <w lemma="man" pos="n2" xml:id="A31646-001-a-0440">Men</w>
       <w lemma="of" pos="acp" xml:id="A31646-001-a-0450">of</w>
      </hi>
      <w lemma="wisdom" pos="n1" xml:id="A31646-001-a-0460">Wisdom</w>
      <w lemma="and" pos="cc" rend="hi" xml:id="A31646-001-a-0470">and</w>
      <w lemma="learning" pos="n1" xml:id="A31646-001-a-0480">Learning</w>
      <pc xml:id="A31646-001-a-0490">,</pc>
     </salute>
    </opener>
    <p xml:id="A31646-e110">
     <w lemma="the" pos="d" xml:id="A31646-001-a-0500">THe</w>
     <w lemma="triple-crowned-little-horn" pos="n1" xml:id="A31646-001-a-0510">TRIPLE-CROWNED-LITTLE-HORN</w>
     <pc xml:id="A31646-001-a-0520">,</pc>
     <w lemma="have" pos="vvz" xml:id="A31646-001-a-0530">hath</w>
     <w lemma="so" pos="av" xml:id="A31646-001-a-0540">so</w>
     <w lemma="worry" pos="vvn" xml:id="A31646-001-a-0550">worried</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-0560">the</w>
     <hi xml:id="A31646-e120">
      <w lemma="saint" pos="n2" xml:id="A31646-001-a-0570">Saints</w>
      <w lemma="of" pos="acp" xml:id="A31646-001-a-0580">of</w>
      <w lemma="the" pos="d" xml:id="A31646-001-a-0590">the</w>
      <w lemma="most" pos="avs-d" xml:id="A31646-001-a-0600">Most</w>
      <w lemma="high" pos="j" xml:id="A31646-001-a-0610">High</w>
      <pc xml:id="A31646-001-a-0620">,</pc>
     </hi>
     <w lemma="out" pos="av" xml:id="A31646-001-a-0630">out</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-0640">of</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-0650">the</w>
     <w lemma="world" pos="n1" xml:id="A31646-001-a-0660">World</w>
     <pc xml:id="A31646-001-a-0670">,</pc>
     <w lemma="with" pos="acp" xml:id="A31646-001-a-0680">with</w>
     <w lemma="his" pos="po" xml:id="A31646-001-a-0690">his</w>
     <w lemma="blasphemous" pos="j" xml:id="A31646-001-a-0700">blasphemous</w>
     <w lemma="endeavour" pos="n2" rend="hi" xml:id="A31646-001-a-0710">Endeavours</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-0720">of</w>
     <w lemma="change" pos="j-vg" xml:id="A31646-001-a-0730">changing</w>
     <w lemma="time" pos="n2" xml:id="A31646-001-a-0740">TIMES</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-0750">and</w>
     <w lemma="law" pos="n2" xml:id="A31646-001-a-0760">LAWS</w>
     <pc xml:id="A31646-001-a-0770">,</pc>
     <w lemma="that" pos="cs" xml:id="A31646-001-a-0780">that</w>
     <w lemma="man" pos="n2" xml:id="A31646-001-a-0790">Men</w>
     <w lemma="know" pos="vvb" xml:id="A31646-001-a-0800">know</w>
     <w lemma="not" pos="xx" xml:id="A31646-001-a-0810">not</w>
     <w lemma="how" pos="crq" xml:id="A31646-001-a-0820">how</w>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-0830">to</w>
     <w lemma="keep" pos="vvi" xml:id="A31646-001-a-0840">keep</w>
     <w lemma="time" pos="n1" rend="hi" xml:id="A31646-001-a-0850">Time</w>
     <w lemma="for" pos="acp" xml:id="A31646-001-a-0860">for</w>
     <w lemma="their" pos="po" xml:id="A31646-001-a-0870">their</w>
     <hi xml:id="A31646-e150">
      <w lemma="law" pos="n2" xml:id="A31646-001-a-0880">Laws</w>
      <pc xml:id="A31646-001-a-0890">,</pc>
     </hi>
     <w lemma="nor" pos="ccx" xml:id="A31646-001-a-0900">nor</w>
     <w lemma="have" pos="vvb" xml:id="A31646-001-a-0910">have</w>
     <w lemma="any" pos="d" xml:id="A31646-001-a-0920">any</w>
     <w lemma="law" pos="n1" rend="hi" xml:id="A31646-001-a-0930">Law</w>
     <w lemma="for" pos="acp" xml:id="A31646-001-a-0940">for</w>
     <w lemma="their" pos="po" xml:id="A31646-001-a-0950">their</w>
     <hi xml:id="A31646-e170">
      <w lemma="Time" pos="n1" xml:id="A31646-001-a-0960">Time</w>
      <pc unit="sentence" xml:id="A31646-001-a-0961">.</pc>
     </hi>
    </p>
    <p xml:id="A31646-e180">
     <w lemma="but" pos="acp" xml:id="A31646-001-a-0980">But</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-0990">the</w>
     <w lemma="learned" pos="j" xml:id="A31646-001-a-1000">Learned</w>
     <w lemma="dr." pos="ab" xml:id="A31646-001-a-1010">Dr.</w>
     <w lemma="more" pos="avc-d" rend="hi" xml:id="A31646-001-a-1020">MORE</w>
     <pc join="right" xml:id="A31646-001-a-1030">(</pc>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-1040">and</w>
     <w lemma="several" pos="j" xml:id="A31646-001-a-1050">several</w>
     <w lemma="other" pos="pi2" xml:id="A31646-001-a-1060">others</w>
     <pc xml:id="A31646-001-a-1070">)</pc>
     <w lemma="in" pos="acp" xml:id="A31646-001-a-1080">in</w>
     <w lemma="their" pos="po" xml:id="A31646-001-a-1090">their</w>
     <w lemma="commentary" pos="n2" xml:id="A31646-001-a-1100">COMMENTARIES</w>
     <w lemma="on" pos="acp" xml:id="A31646-001-a-1110">on</w>
     <hi xml:id="A31646-e200">
      <w lemma="DANIEL" pos="nn1" xml:id="A31646-001-a-1120">DANIEL</w>
      <pc xml:id="A31646-001-a-1130">,</pc>
     </hi>
     <w lemma="have" pos="vvb" xml:id="A31646-001-a-1140">have</w>
     <w lemma="lay" pos="vvn" xml:id="A31646-001-a-1150">laid</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-1160">the</w>
     <w lemma="change" pos="n1" xml:id="A31646-001-a-1170">CHANGE</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-1180">of</w>
     <w lemma="time" pos="n2" xml:id="A31646-001-a-1190">TIMES</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-1200">and</w>
     <w lemma="law" pos="n2" xml:id="A31646-001-a-1210">LAWS</w>
     <w lemma="at" pos="acp" xml:id="A31646-001-a-1220">at</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-1230">the</w>
     <w lemma="door" pos="n2" rend="hi" xml:id="A31646-001-a-1240">Doors</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-1250">of</w>
     <hi xml:id="A31646-e220">
      <w lemma="ROME" pos="nn1" xml:id="A31646-001-a-1260">ROME</w>
      <pc unit="sentence" xml:id="A31646-001-a-1270">.</pc>
     </hi>
     <w lemma="yet" pos="av" xml:id="A31646-001-a-1280">Yet</w>
     <w lemma="because" pos="acp" xml:id="A31646-001-a-1290">because</w>
     <hi xml:id="A31646-e230">
      <pc join="right" xml:id="A31646-001-a-1300">(</pc>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-1310">Dolus</w>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-1320">versatur</w>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-1330">in</w>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-1340">universalibus</w>
      <pc xml:id="A31646-001-a-1350">)</pc>
     </hi>
     <w lemma="they" pos="pns" xml:id="A31646-001-a-1360">they</w>
     <w lemma="seem" pos="vvb" xml:id="A31646-001-a-1370">seem</w>
     <w lemma="not" pos="xx" xml:id="A31646-001-a-1380">not</w>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-1390">to</w>
     <w lemma="be" pos="vvi" xml:id="A31646-001-a-1400">be</w>
     <w lemma="concern" pos="vvn" xml:id="A31646-001-a-1410">concerned</w>
     <pc unit="sentence" xml:id="A31646-001-a-1420">.</pc>
    </p>
    <p xml:id="A31646-e240">
     <w lemma="there" pos="av" xml:id="A31646-001-a-1430">There</w>
     <w lemma="want" pos="vvz" xml:id="A31646-001-a-1440">wants</w>
     <w lemma="a" pos="d" xml:id="A31646-001-a-1450">a</w>
     <w lemma="Nathan" pos="nn1" rend="hi" xml:id="A31646-001-a-1460">NATHAN</w>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-1470">to</w>
     <w lemma="say" pos="vvi" xml:id="A31646-001-a-1480">say</w>
     <pc xml:id="A31646-001-a-1490">,</pc>
     <hi xml:id="A31646-e260">
      <w lemma="thou" pos="pns" xml:id="A31646-001-a-1500">Thou</w>
      <w lemma="be" pos="vv2" xml:id="A31646-001-a-1510">art</w>
      <w lemma="the" pos="d" xml:id="A31646-001-a-1520">the</w>
      <w lemma="man." pos="n1" xml:id="A31646-001-a-1530">Man</w>
      <pc unit="sentence" xml:id="A31646-001-a-1531">.</pc>
     </hi>
     <pc join="right" xml:id="A31646-001-a-1540">(</pc>
     <w lemma="two" pos="crd" xml:id="A31646-001-a-1550">2</w>
     <w lemma="sam." pos="ab" xml:id="A31646-001-a-1560">Sam.</w>
     <w lemma="12.7" pos="crd" xml:id="A31646-001-a-1570">12.7</w>
     <pc xml:id="A31646-001-a-1580">.</pc>
     <pc unit="sentence" xml:id="A31646-001-a-1590">)</pc>
     <w lemma="there" pos="av" xml:id="A31646-001-a-1600">There</w>
     <w lemma="want" pos="vvz" xml:id="A31646-001-a-1610">wants</w>
     <w lemma="some" pos="d" xml:id="A31646-001-a-1620">some</w>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-1630">to</w>
     <w lemma="say" pos="vvi" xml:id="A31646-001-a-1640">say</w>
     <pc xml:id="A31646-001-a-1650">,</pc>
     <w lemma="what" pos="crq" xml:id="A31646-001-a-1660">what</w>
     <hi xml:id="A31646-e270">
      <w lemma="time" pos="n2" xml:id="A31646-001-a-1670">Times</w>
      <pc xml:id="A31646-001-a-1680">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-1690">and</w>
     <w lemma="what" pos="crq" xml:id="A31646-001-a-1700">what</w>
     <hi xml:id="A31646-e280">
      <w lemma="law" pos="n2" xml:id="A31646-001-a-1710">Laws</w>
      <pc xml:id="A31646-001-a-1720">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A31646-001-a-1730">the</w>
     <w lemma="angel" pos="n1" rend="hi" xml:id="A31646-001-a-1740">Angel</w>
     <w lemma="speak" pos="vvd" reg="spoke" xml:id="A31646-001-a-1750">spake</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-1760">of</w>
     <w lemma="to" pos="acp" xml:id="A31646-001-a-1770">to</w>
     <hi xml:id="A31646-e300">
      <w lemma="Daniel" pos="nn1" xml:id="A31646-001-a-1780">Daniel</w>
      <pc unit="sentence" xml:id="A31646-001-a-1790">?</pc>
     </hi>
     <w lemma="dan." pos="ab" xml:id="A31646-001-a-1800">Dan.</w>
     <w lemma="7.25" pos="crd" xml:id="A31646-001-a-1810">7.25</w>
     <pc unit="sentence" xml:id="A31646-001-a-1820">.</pc>
    </p>
    <p xml:id="A31646-e310">
     <w lemma="to" pos="acp" xml:id="A31646-001-a-1830">To</w>
     <w lemma="which" pos="crq" xml:id="A31646-001-a-1840">which</w>
     <w lemma="purpose" pos="n1" xml:id="A31646-001-a-1850">purpose</w>
     <w lemma="I" pos="pns" xml:id="A31646-001-a-1860">I</w>
     <w lemma="do" pos="vvd" xml:id="A31646-001-a-1870">did</w>
     <w lemma="write" pos="vvi" xml:id="A31646-001-a-1880">write</w>
     <w lemma="to" pos="acp" xml:id="A31646-001-a-1890">to</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-1900">the</w>
     <w lemma="most" pos="avs-d" xml:id="A31646-001-a-1910">Most</w>
     <w lemma="reverend" pos="j" rend="hi" xml:id="A31646-001-a-1920">Reverend</w>
     <w lemma="arch-bishop" pos="n2" xml:id="A31646-001-a-1930">ARCH-BISHOPS</w>
     <pc xml:id="A31646-001-a-1940">,</pc>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-1950">and</w>
     <w lemma="bishop" pos="n2" xml:id="A31646-001-a-1960">BISHOPS</w>
     <pc xml:id="A31646-001-a-1970">,</pc>
     <w lemma="on" pos="acp" xml:id="A31646-001-a-1980">on</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-1990">the</w>
     <hi xml:id="A31646-e330">
      <w lemma="king" pos="ng1" xml:id="A31646-001-a-2000">King's</w>
      <w lemma="birthday" pos="n1" reg="Birthday" xml:id="A31646-001-a-2010">Birth-Day</w>
      <pc xml:id="A31646-001-a-2020">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-2030">and</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-2040">the</w>
     <w lemma="most" pos="avs-d" xml:id="A31646-001-a-2050">most</w>
     <w lemma="honourable" pos="j" rend="hi" xml:id="A31646-001-a-2060">Honourable</w>
     <w lemma="judge" pos="n2" xml:id="A31646-001-a-2070">JUDGES</w>
     <w lemma="in" pos="acp" xml:id="A31646-001-a-2080">in</w>
     <w lemma="term" pos="n1" xml:id="A31646-001-a-2090">Term</w>
     <w lemma="time" pos="n1" xml:id="A31646-001-a-2100">time</w>
     <pc xml:id="A31646-001-a-2110">,</pc>
     <w lemma="in" pos="acp" xml:id="A31646-001-a-2120">in</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-2130">the</w>
     <w lemma="very" pos="j" xml:id="A31646-001-a-2140">very</w>
     <w lemma="beginning" pos="n1" xml:id="A31646-001-a-2150">beginning</w>
     <pc unit="sentence" xml:id="A31646-001-a-2160">.</pc>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-2170">And</w>
     <w lemma="now" pos="av" xml:id="A31646-001-a-2180">now</w>
     <w lemma="to" pos="acp" xml:id="A31646-001-a-2190">to</w>
     <w lemma="your" pos="po" xml:id="A31646-001-a-2200">your</w>
     <w lemma="self" pos="n2" rend="hi" xml:id="A31646-001-a-2210">Selves</w>
     <pc join="right" xml:id="A31646-001-a-2220">(</pc>
     <w lemma="against" pos="acp" xml:id="A31646-001-a-2230">against</w>
     <w lemma="your" pos="po" xml:id="A31646-001-a-2240">your</w>
     <w lemma="day" pos="n2" rend="hi" xml:id="A31646-001-a-2250">Days</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-2260">of</w>
     <w lemma="solemnity" pos="n1" rend="hi" xml:id="A31646-001-a-2270">Solemnity</w>
     <pc xml:id="A31646-001-a-2280">)</pc>
     <w lemma="that" pos="cs" xml:id="A31646-001-a-2290">that</w>
     <w lemma="many" pos="d" xml:id="A31646-001-a-2300">many</w>
     <w lemma="hand" pos="n2" xml:id="A31646-001-a-2310">Hands</w>
     <w lemma="may" pos="vmb" xml:id="A31646-001-a-2320">may</w>
     <w lemma="make" pos="vvi" xml:id="A31646-001-a-2330">make</w>
     <w lemma="light" pos="j" xml:id="A31646-001-a-2340">light</w>
     <w lemma="Work" pos="n1" xml:id="A31646-001-a-2350">Work</w>
     <pc unit="sentence" xml:id="A31646-001-a-2351">.</pc>
     <hi xml:id="A31646-e380">
      <w lemma="n/a" pos="ffr" xml:id="A31646-001-a-2360">Plus</w>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-2370">vident</w>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-2380">Oculi</w>
      <pc xml:id="A31646-001-a-2390">,</pc>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-2400">quam</w>
      <w lemma="n/a" pos="fla" xml:id="A31646-001-a-2410">Oculus</w>
      <pc unit="sentence" xml:id="A31646-001-a-2420">.</pc>
     </hi>
    </p>
    <p xml:id="A31646-e390">
     <w lemma="if" pos="cs" xml:id="A31646-001-a-2430">If</w>
     <w lemma="then" pos="av" xml:id="A31646-001-a-2440">then</w>
     <w lemma="you" pos="pn" xml:id="A31646-001-a-2450">you</w>
     <w lemma="be" pos="vvb" xml:id="A31646-001-a-2460">are</w>
     <w lemma="please" pos="vvn" xml:id="A31646-001-a-2470">pleased</w>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-2480">to</w>
     <w lemma="make" pos="vvi" xml:id="A31646-001-a-2490">make</w>
     <w lemma="a" pos="d" xml:id="A31646-001-a-2500">a</w>
     <w lemma="discovery" pos="n1" rend="hi" xml:id="A31646-001-a-2510">Discovery</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-2520">of</w>
     <w lemma="those" pos="d" xml:id="A31646-001-a-2530">those</w>
     <w lemma="time" pos="n2" xml:id="A31646-001-a-2540">TIMES</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-2550">and</w>
     <w lemma="law" pos="n2" xml:id="A31646-001-a-2560">LAWS</w>
     <pc xml:id="A31646-001-a-2570">,</pc>
     <w lemma="you" pos="pn" xml:id="A31646-001-a-2580">you</w>
     <w lemma="do" pos="vvb" xml:id="A31646-001-a-2590">do</w>
     <w lemma="well" pos="av" xml:id="A31646-001-a-2600">well</w>
     <pc xml:id="A31646-001-a-2610">;</pc>
     <w lemma="but" pos="acp" xml:id="A31646-001-a-2620">but</w>
     <w lemma="if" pos="cs" xml:id="A31646-001-a-2630">if</w>
     <w lemma="you" pos="pn" xml:id="A31646-001-a-2640">you</w>
     <w lemma="can" pos="vmb" xml:id="A31646-001-a-2650">can</w>
     <w lemma="restore" pos="vvi" xml:id="A31646-001-a-2660">restore</w>
     <w lemma="they" pos="pno" xml:id="A31646-001-a-2670">them</w>
     <pc xml:id="A31646-001-a-2680">,</pc>
     <w lemma="you" pos="pn" xml:id="A31646-001-a-2690">you</w>
     <w lemma="do" pos="vvb" xml:id="A31646-001-a-2700">do</w>
     <w lemma="better" pos="avc-j" xml:id="A31646-001-a-2710">better</w>
     <pc xml:id="A31646-001-a-2720">:</pc>
     <w lemma="for" pos="acp" xml:id="A31646-001-a-2730">For</w>
     <pc join="right" xml:id="A31646-001-a-2740">(</pc>
     <w lemma="so" pos="av" xml:id="A31646-001-a-2750">so</w>
     <w lemma="extravagant" pos="j" xml:id="A31646-001-a-2760">extravagant</w>
     <w lemma="be" pos="vvz" xml:id="A31646-001-a-2770">is</w>
     <w lemma="yet" pos="av" xml:id="A31646-001-a-2780">yet</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-2790">the</w>
     <w lemma="little-horn" pos="n1" xml:id="A31646-001-a-2800">LITTLE-HORN</w>
     <w lemma="in" pos="acp" xml:id="A31646-001-a-2810">in</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A31646-001-a-2820">England</w>
     <pc xml:id="A31646-001-a-2830">)</pc>
     <w lemma="that" pos="cs" xml:id="A31646-001-a-2840">that</w>
     <w lemma="if" pos="cs" xml:id="A31646-001-a-2850">if</w>
     <w lemma="any" pos="d" xml:id="A31646-001-a-2860">any</w>
     <w lemma="single" pos="j" xml:id="A31646-001-a-2870">Single</w>
     <w lemma="pen" pos="n1" rend="hi" xml:id="A31646-001-a-2880">Pen</w>
     <w lemma="shall" pos="vmd" xml:id="A31646-001-a-2890">should</w>
     <w lemma="draw" pos="vvi" xml:id="A31646-001-a-2900">draw</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-2910">the</w>
     <hi xml:id="A31646-e430">
      <w lemma="curtain" pos="n1" xml:id="A31646-001-a-2920">Curtain</w>
      <pc xml:id="A31646-001-a-2930">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-2940">and</w>
     <w lemma="discover" pos="vvi" xml:id="A31646-001-a-2950">discover</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-2960">the</w>
     <w lemma="nakedness" pos="n1" rend="hi" xml:id="A31646-001-a-2970">Nakedness</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-2980">of</w>
     <w lemma="Antichrist" pos="nn1" xml:id="A31646-001-a-2990">ANTICHRIST</w>
     <w lemma="in" pos="acp" xml:id="A31646-001-a-3000">in</w>
     <w lemma="those" pos="d" xml:id="A31646-001-a-3010">those</w>
     <w lemma="time" pos="n2" rend="hi" xml:id="A31646-001-a-3020">Times</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-3030">and</w>
     <hi xml:id="A31646-e460">
      <w lemma="law" pos="n2" xml:id="A31646-001-a-3040">Laws</w>
      <pc xml:id="A31646-001-a-3050">,</pc>
     </hi>
     <w lemma="they" pos="pns" xml:id="A31646-001-a-3060">they</w>
     <w lemma="will" pos="vmd" xml:id="A31646-001-a-3070">would</w>
     <w lemma="immediate" pos="av-j" xml:id="A31646-001-a-3080">immediately</w>
     <w lemma="bestow" pos="vvi" xml:id="A31646-001-a-3090">bestow</w>
     <w lemma="upon" pos="acp" xml:id="A31646-001-a-3100">upon</w>
     <w lemma="he" pos="pno" xml:id="A31646-001-a-3110">him</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-3120">the</w>
     <w lemma="curse" pos="n1" rend="hi" xml:id="A31646-001-a-3130">Curse</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-3140">of</w>
     <hi xml:id="A31646-e480">
      <w lemma="cham" pos="nn1" xml:id="A31646-001-a-3150">CHAM</w>
      <pc xml:id="A31646-001-a-3160">,</pc>
     </hi>
     <w lemma="or" pos="cc" xml:id="A31646-001-a-3170">or</w>
     <w lemma="adjudge" pos="vvb" xml:id="A31646-001-a-3180">adjudge</w>
     <w lemma="he" pos="pno" xml:id="A31646-001-a-3190">him</w>
     <w lemma="to" pos="acp" xml:id="A31646-001-a-3200">to</w>
     <w lemma="fire" pos="n1" rend="hi" xml:id="A31646-001-a-3210">Fire</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-3220">and</w>
     <hi xml:id="A31646-e500">
      <w lemma="faggot" pos="n1" reg="Faggot" xml:id="A31646-001-a-3230">Fagot</w>
      <pc unit="sentence" xml:id="A31646-001-a-3240">.</pc>
     </hi>
    </p>
    <p xml:id="A31646-e510">
     <w lemma="and" pos="cc" xml:id="A31646-001-a-3250">And</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-3260">the</w>
     <w lemma="scent" pos="n1" xml:id="A31646-001-a-3270">SCENT</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-3280">of</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-3290">the</w>
     <w lemma="horn" pos="n1" xml:id="A31646-001-a-3300">HORN</w>
     <w lemma="be" pos="vvz" xml:id="A31646-001-a-3310">is</w>
     <w lemma="yet" pos="av" xml:id="A31646-001-a-3320">yet</w>
     <w lemma="so" pos="av" xml:id="A31646-001-a-3330">so</w>
     <hi xml:id="A31646-e520">
      <w lemma="rank" pos="j" xml:id="A31646-001-a-3340">Rank</w>
      <pc xml:id="A31646-001-a-3350">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A31646-001-a-3360">that</w>
     <w lemma="it" pos="pn" xml:id="A31646-001-a-3370">it</w>
     <w lemma="be" pos="vvz" xml:id="A31646-001-a-3380">is</w>
     <w lemma="not" pos="xx" xml:id="A31646-001-a-3390">not</w>
     <w lemma="in" pos="acp" xml:id="A31646-001-a-3400">in</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-3410">the</w>
     <w lemma="power" pos="n1" rend="hi" xml:id="A31646-001-a-3420">Power</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-3430">of</w>
     <w lemma="king" pos="n1" xml:id="A31646-001-a-3440">KING</w>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-3450">and</w>
     <w lemma="parliament" pos="n1" xml:id="A31646-001-a-3460">PARLIAMENT</w>
     <pc xml:id="A31646-001-a-3470">,</pc>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-3480">to</w>
     <w lemma="make" pos="vvi" xml:id="A31646-001-a-3490">make</w>
     <w lemma="a" pos="d" xml:id="A31646-001-a-3500">a</w>
     <w lemma="perfect" pos="j" xml:id="A31646-001-a-3510">perfect</w>
     <hi xml:id="A31646-e540">
      <w lemma="restauration" pos="n1" xml:id="A31646-001-a-3520">Restauration</w>
      <pc unit="sentence" xml:id="A31646-001-a-3530">.</pc>
     </hi>
    </p>
    <p xml:id="A31646-e550">
     <w lemma="yet" pos="av" xml:id="A31646-001-a-3540">Yet</w>
     <w lemma="do" pos="vvb" xml:id="A31646-001-a-3550">do</w>
     <w lemma="not" pos="xx" xml:id="A31646-001-a-3560">not</w>
     <w lemma="you" pos="pn" xml:id="A31646-001-a-3570">you</w>
     <w lemma="refuse" pos="vvi" xml:id="A31646-001-a-3580">refuse</w>
     <w lemma="your" pos="po" xml:id="A31646-001-a-3590">your</w>
     <w lemma="help" pos="j-vg" xml:id="A31646-001-a-3600">helping</w>
     <w lemma="hand" pos="n1" xml:id="A31646-001-a-3610">hand</w>
     <pc xml:id="A31646-001-a-3620">—</pc>
    </p>
    <q xml:id="A31646-e560">
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3630">Est</w>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3640">aliquid</w>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3650">prodire</w>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3660">tenus</w>
     <pc xml:id="A31646-001-a-3670">,</pc>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3680">si</w>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3690">non</w>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3700">detur</w>
     <w lemma="n/a" pos="fla" xml:id="A31646-001-a-3710">ultra</w>
     <pc unit="sentence" xml:id="A31646-001-a-3720">.</pc>
    </q>
    <p xml:id="A31646-e570">
     <w lemma="how" pos="crq" xml:id="A31646-001-a-3730">How</w>
     <w lemma="strange" pos="j" xml:id="A31646-001-a-3740">strange</w>
     <w lemma="will" pos="vmd" xml:id="A31646-001-a-3750">would</w>
     <w lemma="it" pos="pn" xml:id="A31646-001-a-3760">it</w>
     <w lemma="appear" pos="vvi" xml:id="A31646-001-a-3770">appear</w>
     <pc xml:id="A31646-001-a-3780">,</pc>
     <w lemma="if" pos="cs" xml:id="A31646-001-a-3790">if</w>
     <w lemma="after" pos="acp" xml:id="A31646-001-a-3800">after</w>
     <w lemma="all" pos="d" xml:id="A31646-001-a-3810">all</w>
     <w lemma="your" pos="po" xml:id="A31646-001-a-3820">your</w>
     <hi xml:id="A31646-e580">
      <w lemma="silence" pos="n1" xml:id="A31646-001-a-3830">Silence</w>
      <pc xml:id="A31646-001-a-3840">,</pc>
     </hi>
     <w lemma="my" pos="po" xml:id="A31646-001-a-3850">my</w>
     <w lemma="goos-quill" pos="n1" rend="hi" xml:id="A31646-001-a-3860">Goos-Quill</w>
     <w lemma="shall" pos="vmd" xml:id="A31646-001-a-3870">should</w>
     <w lemma="make" pos="vvi" xml:id="A31646-001-a-3880">make</w>
     <w lemma="a" pos="d" xml:id="A31646-001-a-3890">a</w>
     <hi xml:id="A31646-e600">
      <w lemma="noise" pos="n1" xml:id="A31646-001-a-3900">Noise</w>
      <pc xml:id="A31646-001-a-3910">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A31646-001-a-3920">and</w>
     <w lemma="awaken" pos="vvi" xml:id="A31646-001-a-3930">awaken</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-3940">the</w>
     <w lemma="capitol" pos="n1" xml:id="A31646-001-a-3950">CAPITOL</w>
     <w lemma="of" pos="acp" xml:id="A31646-001-a-3960">of</w>
     <hi xml:id="A31646-e610">
      <w lemma="ENGLAND" pos="nn1" xml:id="A31646-001-a-3970">ENGLAND</w>
      <pc xml:id="A31646-001-a-3980">,</pc>
     </hi>
     <w lemma="to" pos="prt" xml:id="A31646-001-a-3990">to</w>
     <w lemma="prevent" pos="vvi" xml:id="A31646-001-a-4000">prevent</w>
     <w lemma="a" pos="d" xml:id="A31646-001-a-4010">a</w>
     <pc join="right" xml:id="A31646-001-a-4020">(</pc>
     <w lemma="total" pos="j" xml:id="A31646-001-a-4030">total</w>
     <pc xml:id="A31646-001-a-4040">)</pc>
     <w lemma="desolation" pos="n1" rend="hi" xml:id="A31646-001-a-4050">Desolation</w>
     <w lemma="prophesy" pos="vvn" xml:id="A31646-001-a-4060">prophesied</w>
     <w lemma="by" pos="acp" xml:id="A31646-001-a-4070">by</w>
     <w lemma="the" pos="d" xml:id="A31646-001-a-4080">the</w>
     <hi xml:id="A31646-e630">
      <w lemma="lord" pos="n1" xml:id="A31646-001-a-4090">Lord</w>
      <w lemma="Jesus" pos="nn1" xml:id="A31646-001-a-4100">Jesus</w>
      <w lemma="Christ" pos="nn1" xml:id="A31646-001-a-4110">Christ</w>
      <pc xml:id="A31646-001-a-4120">,</pc>
     </hi>
     <pc join="right" xml:id="A31646-001-a-4130">(</pc>
     <w lemma="mat." pos="ab" xml:id="A31646-001-a-4140">Mat.</w>
     <w lemma="12.25" pos="crd" xml:id="A31646-001-a-4150">12.25</w>
     <pc xml:id="A31646-001-a-4160">.</pc>
     <pc unit="sentence" xml:id="A31646-001-a-4170">)</pc>
    </p>
    <p xml:id="A31646-e640">
     <w lemma="let" pos="vvb" xml:id="A31646-001-a-4180">Let</w>
     <w lemma="not" pos="xx" xml:id="A31646-001-a-4190">not</w>
     <w lemma="this" pos="d" xml:id="A31646-001-a-4200">this</w>
     <w lemma="service" pos="n1" rend="hi" xml:id="A31646-001-a-4210">Service</w>
     <w lemma="be" pos="vvi" xml:id="A31646-001-a-4220">be</w>
     <w lemma="unseasonable" pos="j" xml:id="A31646-001-a-4230">unseasonable</w>
     <w lemma="from" pos="acp" xml:id="A31646-001-a-4240">from</w>
    </p>
    <closer xml:id="A31646-e660">
     <dateline xml:id="A31646-e670">
      <date xml:id="A31646-e680">
       <w lemma="June" pos="nn1" xml:id="A31646-001-a-4250">June</w>
       <w lemma="19" pos="crd" xml:id="A31646-001-a-4260">19</w>
       <pc xml:id="A31646-001-a-4270">,</pc>
       <w lemma="1682." pos="crd" xml:id="A31646-001-a-4280">1682.</w>
       <pc unit="sentence" xml:id="A31646-001-a-4290"/>
      </date>
     </dateline>
     <signed xml:id="A31646-e690">
      <hi xml:id="A31646-e700">
       <w lemma="your" pos="po" xml:id="A31646-001-a-4300">Your</w>
       <w lemma="most" pos="avs-d" xml:id="A31646-001-a-4310">most</w>
       <w lemma="humble" pos="j" xml:id="A31646-001-a-4320">humble</w>
       <w lemma="christian" pos="jnn" xml:id="A31646-001-a-4330">Christian</w>
       <w lemma="servant" pos="n1" xml:id="A31646-001-a-4340">Servant</w>
      </hi>
      <w lemma="Peter" pos="nn1" xml:id="A31646-001-a-4350">Peter</w>
      <w lemma="chamberlain" pos="n1" reg="Chamberlain" xml:id="A31646-001-a-4360">Chamberlen</w>
      <pc unit="sentence" xml:id="A31646-001-a-4370">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A31646-t-b"/>
 </text>
</TEI>
